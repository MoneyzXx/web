###### 玩转金刚>金刚字典>
### 金刚副号
- <strong> 金刚副号 </strong >是[ 金刚主号 ](/LadderFree/kkDictionary/KKIDMain.md)的相对称谓
- [ 金刚 ](/LadderFree/kkDictionary/Atozitpro.md)把具有以下特征的[ 金刚号 ](/LadderFree/kkDictionary/KKID.md)叫<Strong> 金刚副号 </Strong >：
  - 非c9开头
  - 其上捆绑免费[ 小额流量 ](/LadderFree/kkDictionary/KKDataTrafficSmallAmount.md)包
  - 同一时间您只可持有1个
  - 偶尔连通使用


#### 返回到
- [玩转金刚梯](/LadderFree/A.md)
- [金刚字典](/LadderFree/kkDictionary/KKDictionary.md)


